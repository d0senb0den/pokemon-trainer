# Pokémon Trainer
##### This is a simple Angular web application where you can sign in with just a username. The user will then be redirected to the pokemon catalogue where the user is able to 'catch' pokemons by clicking on the individual cards. The 'catched' pokemons will then be added to the peronal trainer page. When navigating to the trainer page the user will be presented with all the pokemons that has been currently catched.
## Install
#### 1. Fork and Clone repository
#### 2. Install Angular CLI
#### 3. Install NPM
#### 4. In the "Environments" folder, open "Environments.ts" Enter ApiKey and ApiUsers from Heroku.com and apiPokemon from any pokemon API.
##### (You can find/create your key in settings -> Config vars. Tips: Create key using a guid generator like guidgenerator.com)
##### (Press button "Open App" then "Trainers" and copy Url)
```
(Example of Environments.ts file)

export const environment = {
  production: false,
  apiUsers: "https://pontus-noroff-api.herokuapp.com/trainers",
  apiKey: "<place api key here>",
  apiPokemon: "https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0"
};
``` 
#### 5. Now it's good to go! Type "ng serve" in terminal to run the website! Good luck 🙂
## Contributors
##### [Jeremy Matthiessen (@jerry585)](@jerry585)
##### [Pontus Gillson (@d0senb0den)](@d0senb0den)
