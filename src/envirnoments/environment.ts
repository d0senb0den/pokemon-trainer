export const environment = {
  production: false,
  apiUsers: "https://pontus-noroff-api.herokuapp.com/trainers",
  apiKey: "",

  apiPokemon: "https://pokeapi.co/api/v2/pokemon/"
};