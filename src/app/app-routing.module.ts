import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { HomePage } from "./pages/home/home.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { TrainerPage } from "./pages/trainer/trainer.page";

const routes: Routes = [
    {
        path: "home",
        component: HomePage
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [ AuthGuard ]
    },
    {
        path: "catalogue",
        component: PokemonCataloguePage,
        canActivate: [ AuthGuard ]
    },
    {
        path: "",
        redirectTo: '/home',
        pathMatch: 'full'
    }

    // add to other pages: canActivate: [ AuthGuard ]
    // Checks that user is logged in, otherwise sends to /home
]


@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]

})
export class AppRoutingModule{

}