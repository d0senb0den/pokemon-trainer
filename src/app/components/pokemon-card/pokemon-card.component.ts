import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { never } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CaughtPokemonService } from 'src/app/services/caught-pokemon.service';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon?: Pokemon
  @Input() pokemonId: number = 0// Pokemon ID = 0 does not exist

  get loading(): boolean {
    return this.pokemonService.loading
  }

  constructor(
    private readonly pokemonService: CaughtPokemonService
  ) { }

  ngOnInit(): void {
  }

  onCatchClick(id: number): void {
    this.pokemonId = id
    this.pokemonService.addToCaught(this.pokemonId)
    .subscribe({
      next: (response: User) => {
        console.log("NEXT", response),
        alert(`Congratulations! You just caught: ${this.pokemon?.name}`)
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message)
      }
    })
  }

}
