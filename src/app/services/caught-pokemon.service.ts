import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, User } from '../models/user.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';

const {apiKey, apiUsers} = environment

@Injectable({
  providedIn: 'root'
})
export class CaughtPokemonService {

  private _loading: boolean = false

  get loading(): boolean {
    return this._loading
  }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService
  ) { }

  public addToCaught(pokemonId: number): Observable<User> {
    if (!this.userService.user) {
      throw new Error("addToCaught: There is no user");
      
    }

    const user: User = this.userService.user

    const pokemon: Pokemon | undefined = this.pokemonService.pokemonByName(pokemonId)

    if (!pokemon) {
      throw new Error("addToCaught: No pokemon with name: " + pokemonId);
    }

    if (this.userService.inCaught(pokemonId)) {
      throw new Error("addToCaught: Pokemon already caught");
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key' : apiKey
    })

    this._loading = true

    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
      pokemon: [...user.pokemon, pokemon] 
    }, {
      headers
    })
    .pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser
      }),
      finalize(() =>{
        this._loading = false
      })
    )
  }
}
