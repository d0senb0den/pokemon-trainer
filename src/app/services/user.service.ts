import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _pokemons: Pokemon[] = []
  private _user?: User

  get pokemons(): Pokemon[] {
    return this._pokemons
  }

  get user(): User | undefined {
    return this._user
  }

  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
    this._user = user
  }

  constructor(
    private readonly _http:HttpClient
  ) { 
    this._user = StorageUtil.storageRead<User>(StorageKeys.User)
  }
  public inCaught(pokemonId: number): boolean{
    if (this._user) { 
      return Boolean(this.user?.pokemon.find((pokemon: any) => pokemon.id === pokemonId))
    }
    return false
  }

  
  public getCaughtPokemons(){
    let username: any = sessionStorage.getItem("pokemon-user")
    //console.log(this._user?.username);
    this._http.get<User[]>(`${apiUsers}?id=${this._user?.id}`)
    .subscribe((res: any) => {
      //console.log(res[0].pokemon);
      let tmp: any[] = res[0].pokemon
      let tmpPokemon: any[] = []
      for (let i = 0; i < tmp.length; i++) {
        console.log(tmp[i].name);
        this._http.get<Pokemon>("https://pokeapi.co/api/v2/pokemon/" + tmp[i].name)
        .subscribe((pokemon: any) => {
          tmpPokemon[i] = {id: pokemon.id, name: pokemon.name, sprite: pokemon.sprites.front_default}
          this._pokemons = tmpPokemon
        })
      }
    })
    //.pipe(map((response: User[]) => response.pop()))
    //this._http.get<Pokemon>("https://pokeapi.co/api/v2/pokemon/")
  }
}
