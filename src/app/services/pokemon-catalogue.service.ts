import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';

const { apiPokemon } = environment

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = []
  private _error: string = ""
  private _loading: boolean = true

  get pokemons(): Pokemon[] {
    return this._pokemons
  }

  get error(): string {
    return this._error
  }

  get loading(): boolean {
    return this._loading
  }

  constructor(private readonly _http:HttpClient) { }

  /**
   * Gets all pokemons with it's details.
   */
  public getAllPokemon() : void {
    this._http.get<PokemonResponse>(apiPokemon) // Get the pokemon list
    .subscribe((res: PokemonResponse) => {
      console.log(res);
      let results:any = res.results
      let tmp:Pokemon[] = []       
      for (let i = 0; i < res.count; i++) {
        this._http.get<Pokemon[]>(results[i].url) // Get the pokemon details
        .pipe(
          finalize(() => {
            this._loading = false;
          })
        )
        .subscribe((pokemon: any) => {
          tmp[i] = {id: pokemon.id, name: pokemon.name, sprite: pokemon.sprites.front_default}
          this._pokemons = tmp
          StorageUtil.storageSave("pokemons", this._pokemons)
        })
      }
    })
  }
  public getStoredPokemon(): void {
    let temp: any = sessionStorage.getItem("pokemons")
    let tmp:Pokemon[] = []
    for (let i = 0; i < temp?.length; i++) {
      tmp[i] = {id: temp.id, name: temp.name, sprite: temp.sprites.front_default}
          this._pokemons = tmp
          console.log(tmp[i]);
          
    }
  }
  public pokemonByName(id: number): Pokemon | undefined {
    return this._pokemons.find((pokemon : Pokemon) => pokemon.id === id)
  }
}
