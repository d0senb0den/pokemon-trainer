import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CaughtPokemonService } from 'src/app/services/caught-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  constructor(
    private readonly _trainerService: UserService
  ) { }

  get pokemons(): Pokemon[] {
    return this._trainerService.pokemons
  }

  ngOnInit(): void {
    this._trainerService.getCaughtPokemons()
  }

}
