import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): Pokemon[] {
    return this._pokemonCatalogService.pokemons
  }

  get error(): string {
    return this._pokemonCatalogService.error
  }

  get loading(): boolean {
    return this._pokemonCatalogService.loading
  }

  constructor(private readonly _pokemonCatalogService:PokemonCatalogueService) { }

  ngOnInit(): void {
    // if (!sessionStorage.getItem("pokemon")) {
    // } else {
    //   this._pokemonCatalogService.getStoredPokemon()
    // }
    this._pokemonCatalogService.getAllPokemon()
  }

}
