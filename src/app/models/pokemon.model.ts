export interface Results {
    name:string,
    url:string
}

export interface Pokemon{
    name: string,
    id: number,
    sprite: Sprite
}

interface Sprite{
    front_default: string
}

export interface PokemonResponse {
    count: number,
    next: boolean,
    previous: boolean,
    results: Results
}