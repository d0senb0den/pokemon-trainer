export interface User{
    id: number;
    username: string;
    pokemon: Pokemon[];
}

export interface Pokemon{
    name: string
}