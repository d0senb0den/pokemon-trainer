/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'darkBlue': '#032B43',
        'lightBlue': '#3F88C5',
        'red': '#D00000'
      }
    },
  },
  plugins: [],
}
